import React, { Component } from 'react';
import '../css/Sidebar.css'
import {explore,map}  from '../img/icons'
class Sidebar extends Component {
    render() {
        return (
            <div className="sidebar">
                    <h2>Discover Videos</h2>
                    <ul>
                        <li><img src={explore}/> <span>Explore</span> </li>
                        <li><img src={map}/> <span>Map</span> </li>
                    </ul>
            </div>
        );
    }
}
export default Sidebar;