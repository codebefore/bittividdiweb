import React, { Component } from 'react';
import '../css/Header.css'
import {logo,profile} from '../img/icons'
class Header extends Component {
    render() {
        return (
               <div className="header">
                   <img src={logo} className="logo"/>
                   <img src={profile} className="profile"/>
                   <span className="hiuser">Hi, Daniel</span>
                   <div className="circle"></div>
                   <div className="circle"></div>
               </div>
        );
    }
}
export default Header;