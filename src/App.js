import React from 'react';
import './App.css';

// import { Link, Route } from 'react-router-dom';
// import MoviesPage from './pages/MoviesPage';
import { Container,Col,Row } from 'reactstrap';
import Header from './pages/Header';
import Sidebar from './pages/Sidebar';
import Content from './pages/Content';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
    return ( 
        
        <div>
            <Container fluid={true}>
                 <Row>
                     <Col><Header /></Col>
                    
                 </Row>
                 <Row>
                    <Col sm={3}><Sidebar/></Col>
                    <Col sm={9}></Col>
                </Row>
            </Container>
            {/*<Content/> */}
            {/* <Link to = "movies" > Movies </Link>
            <Route path = "/movies" component = { MoviesPage } > </Route> */}
        </div>
    );
}
export default App;